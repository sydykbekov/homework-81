import axios from 'axios';
export const START_REQUEST = 'START_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';

const startRequest = () => {
    return {type: START_REQUEST};
};

const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

const successRequest = object => {
    return {type: SUCCESS_REQUEST, object};
};

export const shorten = link => {
    return dispatch => {
        dispatch(startRequest());
        axios.post('', link).then(response => {
            dispatch(successRequest(response.data))
        }, error => {
            dispatch(errorRequest());
        })
    }
};

export const redirectToOriginalLink = () => {
    return (dispatch, getState) => {
        axios.get(`${getState().shortURL}`);
    };
};