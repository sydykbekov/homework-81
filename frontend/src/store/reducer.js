import {ERROR_REQUEST, START_REQUEST, SUCCESS_REQUEST} from "./action";

const initialState = {
    shortURL: '',
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case START_REQUEST:
            return {...state, loading: true};
        case ERROR_REQUEST:
            return {...state, loading: false};
        case SUCCESS_REQUEST:
            return {...state, loading: false, shortURL: action.object.shortURL};
        default:
            return state;
    }
};

export default reducer;