import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormControl, FormGroup, Panel} from "react-bootstrap";
import {redirectToOriginalLink, shorten} from "./store/action";
import './App.css';
import Preloader from './assets/Preloader.gif';

const URL = 'http://localhost:8000/';

class App extends Component {
    state = {
        link: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    shorten = event => {
        event.preventDefault();
        this.props.shorten(this.state);
    };

    render() {
        return (
            <Panel style={{textAlign: 'center'}}>
                <img src={Preloader} alt="loading..." style={{display: this.props.loading ? 'block' : 'none'}}/>
                <Panel.Body>
                    <h3>Shorten your link!</h3>
                    <Form horizontal onSubmit={this.shorten}>
                        <FormGroup controlId="commentAuthor">
                            <Col sm={12}>
                                <FormControl
                                    type="url" required
                                    placeholder="URL ..."
                                    name="link"
                                    value={this.state.link}
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col>
                                <Button bsStyle="primary" type="submit">Shorten!</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                    {this.props.link && <h4>Your link now looks like this:</h4>}
                    {this.props.link && <a href={URL + this.props.link} target="_blank" className="redirect" onClick={this.props.redirectToOriginalLink}>{URL + this.props.link}</a>}
                </Panel.Body>
            </Panel>
        );
    }
}

const mapStateToProps = state => {
    return {
        link: state.shortURL,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        shorten: (state) => dispatch(shorten(state)),
        redirectToOriginalLink: () => dispatch(redirectToOriginalLink())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
