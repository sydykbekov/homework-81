const mongoose = require('mongoose');

const LinkSchema = new mongoose.Schema({
    link: {type: String, required: true},
    shortURL: String
});

const Link = mongoose.model('Product', LinkSchema);

module.exports = Link;