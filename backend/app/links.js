const express = require('express');
const nanoid = require('nanoid');
const Link = require('../models/Link');

const router = express.Router();

const createRouter = () => {
    router.get('/', (req, res) => {
        Link.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', (req, res) => {
        const productData = req.body;

        productData.shortURL = nanoid(7);

        Link.findOne({shortURL: productData.shortURL}).then(result => {
             if (result) {
                 productData.shortURL = nanoid(8);

                 const product = new Link(productData);

                 product.save()
                     .then(() => res.send(product))
                     .catch(error => res.status(400).send(error));
             } else {
                 const product = new Link(productData);

                 product.save()
                     .then(() => res.send(product))
                     .catch(error => res.status(400).send(error));
             }
        });
    });

    router.get('/:shortURL', (req, res) => {
        const shortURL = req.params.shortURL;
        Link.findOne({shortURL: shortURL})
            .then(result => {
                if (result) res.status(301).redirect(result.link);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;